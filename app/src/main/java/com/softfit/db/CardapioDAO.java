package com.softfit.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.softfit.modelagem.Cardapio;

import java.util.ArrayList;

public class CardapioDAO extends Banco{

    private Context context;

    public CardapioDAO(Context context) {
        super(context);
        this.context = context;
    }

    public ContentValues obterDadosCardapio(Cardapio cardapio){
        ContentValues values = new ContentValues();
        values.put("car_descricao", cardapio.getDescricao());
        values.put("car_ref_id", cardapio.getRefeicaoId());
        return values;
    }

    public void salvarCardapio (Cardapio cardapio){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = obterDadosCardapio(cardapio);
        db.insert("Cardapios",null, values);
    }

    public void deletarTodosOsCardapios() {
        getWritableDatabase().execSQL("DELETE FROM Cardapios");
        new RefeicoesDAO(context).deletarTodasRefeicoes();
    }

    public ArrayList<Cardapio> recuperarCardapios(int id_refeicao){
        ArrayList <Cardapio> cardapios = new ArrayList<>();

        try{
            String[] argumentos = { id_refeicao + ""};
            Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM cardapios WHERE id=?", argumentos);
            while (cursor.moveToNext()){
                cardapios.add(new Cardapio(
                        cursor.getInt(cursor.getColumnIndex("car_id")),
                        cursor.getString(cursor.getColumnIndex("car_descricao")),
                        cursor.getInt(cursor.getColumnIndex("car_ref_id"))
                ));
            }
        }catch (SQLiteException e){}
        return cardapios;
    }
}