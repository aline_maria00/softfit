package com.softfit.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.softfit.modelagem.Usuario;

public class UsuarioDAO extends Banco {

    private Context context;

    public UsuarioDAO(Context context) {
        super(context);
        this.context = context;
    }

    public ContentValues montarValores (Usuario usuario){
        ContentValues valores = new ContentValues();
        valores.put("usu_nome", usuario.getNome());
        valores.put("usu_idade", usuario.getIdade());
        valores.put("usu_peso", usuario.getPeso());
        valores.put("usu_altura", usuario.getAltura());
        valores.put("usu_localizacao", usuario.getLocalizacao());
        return valores;
    }
    public void salvarValores (Usuario usuario){
        SQLiteDatabase sql = getWritableDatabase();
        ContentValues valores = montarValores(usuario);
        sql.insert("UsuarioDAO", null, valores);

    }
    public Usuario buscarUsuario(){
        Usuario usuario = null;
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM Usuarios", null);

            if (cursor.moveToFirst()){
                usuario = new Usuario(
                        cursor.getString(cursor.getColumnIndex("usu_nome")),
                        cursor.getInt(cursor.getColumnIndex("usu_idade")),
                        cursor.getFloat(cursor.getColumnIndex("usu_peso")),
                        cursor.getFloat(cursor.getColumnIndex("usu_altura")),
                        cursor.getString(cursor.getColumnIndex("usu_localizacao"))
                );
            }
        return usuario;
    }

    public void editarUsuario (Usuario usuario){
        ContentValues dados = this.montarValores(usuario);
        getWritableDatabase().update("Usuatios", dados, "where id=" + usuario.getIdUsuario() + "", null);
    }
}
