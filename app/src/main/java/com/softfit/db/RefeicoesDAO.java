package com.softfit.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.softfit.modelagem.Refeicao;

import java.util.ArrayList;

public class RefeicoesDAO extends Banco {

    public RefeicoesDAO(Context context) {
        super(context);
    }

    public ContentValues obterDadosRefeicoes(Refeicao refeicao){
        ContentValues values = new ContentValues();
        values.put("ref_horario", refeicao.getHora());
        values.put("ref_nome", refeicao.getNome());
        values.put("ref_img_url", "");
        return values;
    }

    public void salvarRefeicao (Refeicao refeicao){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = obterDadosRefeicoes(refeicao);
        db.insert("Refeicoes",null, values);
    }

    public ArrayList<Refeicao> recuperarRefeicoes() {
        ArrayList<Refeicao> refeicoes = new ArrayList<>();

        try {
            Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM Refeicoes;", null);

            while(cursor.moveToNext()){ //PEGAR TODOS OS DADOS E COLOCAR NO ARRAYLIST
                refeicoes.add(new Refeicao(
                        cursor.getInt(cursor.getColumnIndex("ref_id")),
                        cursor.getString(cursor.getColumnIndex("ref_nome")),
                        cursor.getString(cursor.getColumnIndex("ref_horario")),
                        cursor.getString(cursor.getColumnIndex("ref_img_url"))
                ));
                Log.i("Script", "A");
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        return refeicoes;
    }

    public void deletarTodasRefeicoes() {
        getWritableDatabase().execSQL("DELETE FROM Refeicoes;");
    }

    public void atualizarImagemDaRefeicao(int id, String caminhoDaImagem) {
        ContentValues values = new ContentValues();
        values.put("ref_img_url", caminhoDaImagem);

        String[] argumentos = { id + "" };

        getWritableDatabase().update("Refeicoes", values, "ref_id=?", argumentos);
    }

}
