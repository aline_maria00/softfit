package com.softfit.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Banco extends SQLiteOpenHelper {

    public Banco(Context context){
        super(context,"Softfit",null, 11);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(
                "create table if not exists Refeicoes(" +
                        "ref_id integer primary key autoincrement not null," +
                        "ref_nome varchar(60) not null," +
                        "ref_horario varchar(10)," +
                        "ref_img_url text" +
                ");"
        );
        db.execSQL(
                "create table if not exists Cardapios(" +
                        "car_id integer primary key autoincrement not null," +
                        "car_descricao text not null," +
                        "car_ref_id text not null," +
                        "foreign key (car_ref_id) references Refeicoes(ref_id)" +
                ");"
        );
        db.execSQL(
                "create table if not exists Registros(" +
                        "reg_id integer primary key autoincrement not null," +
                        "reg_data varchar(15) not null," +
                        "reg_img_url TEXT NOT NULL," +
                        "reg_car_id integer not null," +
                        "foreign key (reg_car_id) references Cardapios(car_id)" +
                ");"
        );
        db.execSQL(
                "create table if not exists Usuarios("
                + "usu_id integer primary key autoincrement not null,"
                + "usu_nome TEXT NOT NULL,"
                + "usu_idade integer NOT NULL,"
                + "usu_peso real NOT NULL,"
                + "usu_altura real NOT NULL,"
                + "usu_localizacao TEXT NOT NULL"
                + ");"
        );

    }

    public void onUpgrade(SQLiteDatabase db, int versaoAntiga, int versaoNova){
        db.execSQL("drop table Refeicoes;");
        db.execSQL("drop table Cardapios;");
        db.execSQL("drop table Registros;");
        db.execSQL("drop table if exists Usuarios;");
        onCreate(db);
    }

}


