package com.softfit.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.softfit.modelagem.Registro;

import java.util.ArrayList;

public class RegistroDAO extends Banco{

    private Context context;

    public RegistroDAO(Context context) {
        super(context);
        this.context = context;
    }


    public ContentValues obterDados (Registro registro){
        ContentValues valores = new ContentValues();
        valores.put("reg_id", registro.getId());
        valores.put("reg_img_url", registro.getImagem());
        return valores;
    }

    public void salvarRegistro (Registro registro){
        SQLiteDatabase sql = getWritableDatabase();
        ContentValues valores = obterDados(registro);
        sql.insert("Registro", null, valores);

    }

    public ArrayList<Registro> recuperarRegistro(){
        ArrayList <Registro> lista = new ArrayList <>();

        //try {
        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM Registros;", null);

        while(cursor.moveToNext()){
            lista.add(new Registro(
                    cursor.getInt(cursor.getColumnIndex("reg_id")),
                    cursor.getString(cursor.getColumnIndex("reg_img_url"))
            ));
        }
//
//        } catch(Exception e) {
//            e.printStackTrace();
//        }

        return lista;
    }

    public void deletarTodosOsRegistros() {
        getWritableDatabase().execSQL("DELETE FROM REGISTROS;");
        new CardapioDAO(context).deletarTodosOsCardapios();
    }
}
