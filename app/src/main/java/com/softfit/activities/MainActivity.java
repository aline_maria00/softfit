package com.softfit.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.softfit.db.CardapioDAO;
import com.softfit.db.RefeicoesDAO;
import com.softfit.BuildConfig;
import com.softfit.modelagem.Cardapio;
import com.softfit.R;
import com.softfit.modelagem.Refeicao;
import com.softfit.modelagem.Usuario;
import com.softfit.utils.RefeicaoAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RefeicaoAdapter adaptador;

    private RefeicoesDAO refeicoesDAO;
    private String caminhoFoto;
    public static final int REQUEST_CODE_CAMERA = 123;

    private ImageView foto;

    private Refeicao refSelecionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Softfit");

        //recuperando as refeiçoes por meio do classe refeicaoAdapter
        refeicoesDAO = new RefeicoesDAO(this);
        adaptador = new RefeicaoAdapter(this, refeicoesDAO.recuperarRefeicoes());

        //pegar listview
        //definir adapter;
        //registrar para menu_de_contexto;

        ListView list_view = findViewById(R.id.listViewRefeicoes);
        list_view.setAdapter(adaptador);
        registerForContextMenu(list_view);

        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showPopUp(i);
                refSelecionada = (Refeicao) adaptador.getItem(i);
            }
        });

        FloatingActionButton novaRefeicao = findViewById(R.id.fab);
        novaRefeicao.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, CadastroDeRefeicao.class);
                        startActivity(intent);
                    }
                }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {    //Aline
        getMenuInflater().inflate(R.menu.menu_opcoes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {     //Aline

        if(item.getItemId() == R.id.dados_de_usuario){
            Intent intent = new Intent(MainActivity.this, Usuario.class);
            startActivity(intent);
        }else if(item.getItemId() == R.id.sobreApp){
            Intent intent = new Intent(MainActivity.this, Sobre.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTitle(String title){     //Aline
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView textView = new TextView(this);
        textView.setText(title);
        textView.setTextSize(30);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(getResources().getColor(R.color.pretao));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(textView);
    }

    public void showPopUp(int position) {
        View v = getLayoutInflater().inflate(R.layout.pop_up, null, false);

        Refeicao ref = (Refeicao) adaptador.getItem(position);
        ArrayList<Cardapio> cardapios = new CardapioDAO(this).recuperarCardapios(ref.getId());

        TextView tituloDaRefeicao = v.findViewById(R.id.tituloAlimentacao);
        Spinner spinnerCardapio = v.findViewById(R.id.spinnerCardapio);
        foto = v.findViewById(R.id.imagemTitulo);
        Button btnAdicionarFoto = v.findViewById(R.id.botao_add_foto);

        btnAdicionarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                caminhoFoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
                File file = new File(caminhoFoto);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(
                                MainActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                file
                        ));

                startActivityForResult(intent, 123);
            }
        });

        tituloDaRefeicao.setText(ref.getNome());
        Bitmap bitmap = BitmapFactory.decodeFile(ref.getImg());
        foto.setImageBitmap(bitmap);
        foto.setTag(ref.getImg());

        ArrayAdapter adaptadorDoSpinner = new ArrayAdapter(this, android.R.layout.simple_list_item_1, cardapios);
        adaptadorDoSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCardapio.setAdapter(adaptadorDoSpinner);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(v);
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        adaptador.atualizar(refeicoesDAO.recuperarRefeicoes());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setImage(caminhoFoto);
    }

    private void setImage(String caminhoDaFoto) {
        Log.i("Script", "Caminho da foto: " + caminhoDaFoto);
        Bitmap bitmap = BitmapFactory.decodeFile(caminhoDaFoto);
        foto.setImageBitmap(bitmap);
        foto.setTag(caminhoDaFoto);
        refeicoesDAO.atualizarImagemDaRefeicao(refSelecionada.getId(), caminhoDaFoto);
    }
}