package com.softfit.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.softfit.db.CardapioDAO;
import com.softfit.db.RefeicoesDAO;
import com.softfit.modelagem.Cardapio;
import com.softfit.R;
import com.softfit.modelagem.Refeicao;

import java.util.ArrayList;

public class CadastroDeCardapio extends AppCompatActivity {
    private TextView tvNomeRefeicao;
    private EditText edtCardapio;
    private int index;
    private ArrayList<Refeicao> refeicoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_cardapio);
        index = 0;
        refeicoes = new RefeicoesDAO(this).recuperarRefeicoes();
        setTitle("Cadastro de cardápio");


        tvNomeRefeicao = findViewById(R.id.textViewNomeRefeicao);
        edtCardapio = findViewById(R.id.editTextCardapio);
        Log.i("teste", refeicoes.toString());

        if(refeicoes.isEmpty()){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Refeições não cadastradas!!!");
            alert.setTitle("ERROR!");
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.show();
        }
        else {
            ajustarRefeicoes();
        }
    }

    private boolean next() {
        salvarDados();
        index++;
        if (index >= refeicoes.size())
            return false;
        ajustarRefeicoes();
        return true;
    }

    private void ajustarRefeicoes() {
        edtCardapio.setText("");
        tvNomeRefeicao.setText(refeicoes.get(index).getNome());
    }

    private void salvarDados() {
        CardapioDAO cardapioDAO = new CardapioDAO(getBaseContext());
        String stringCardapio = edtCardapio.getText().toString();
        String[] cardapios = stringCardapio.split(";");

        for(int i = 0; i < cardapios.length;i++){
            cardapioDAO.salvarCardapio(new Cardapio(cardapios[i], refeicoes.get(index).getId()));
        }
    }

    public void onClickFAB(View view) {
        if(!next()) {
            finish();
        }
    }
}