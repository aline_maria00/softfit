package com.softfit.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.softfit.db.RefeicoesDAO;
import com.softfit.db.RegistroDAO;
import com.softfit.R;
import com.softfit.modelagem.Refeicao;

import java.util.ArrayList;
import java.util.Calendar;

public class CadastroDeRefeicao extends AppCompatActivity {

    ArrayList<EditText> inputsNome = new ArrayList<>();
    ArrayList<TextView> inputsHorario = new ArrayList<>();

    private RefeicoesDAO refDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_refeicao);

        new RegistroDAO(this).deletarTodosOsRegistros();

        refDAO = new RefeicoesDAO(this);

        setTitle("Cadastro de refeição"); //altera o título da barrinha


        FloatingActionButton adicionarCardapio = findViewById(R.id.fabOkRefeicao);

        adicionarCardapio.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //cadastro de refeições
                        for (int i=0; i<inputsNome.size(); i++){
                            Refeicao ref = new Refeicao(0, inputsNome.get(i).getText().toString(), inputsHorario.get(i).getText().toString(), "");

                            refDAO.salvarRefeicao(ref);
                            Log.i("Script", "Refeição salva com sucesso!");
                        }

                        Intent intent = new Intent(CadastroDeRefeicao.this, CadastroDeCardapio.class);
                        startActivity(intent);

                        finish();
                    }
                }
        );

        Spinner quantidade = findViewById(R.id.spinnerQuantidade); //declaração do spinner

        quantidade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { //definição da ação de click do item do Spinner quantidade
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View itemSelecionado, int posicao, long l) { //no caso desse spinner ele precisa de algumas variáveis como parâmetro, uma é o adater d próprio spinner, a outra é
                String item = (String) adapterView.getAdapter().getItem(posicao); //pegando o item do adaptador na posição do item selecionado

                atualizarListaDeInputs(Integer.parseInt(item)); //chamando o método que cria os inputs conforme a quantidade selecionada
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { //método ativado quando nenhum item for selecionado no spinner

            }
        });
    }

    public void atualizarListaDeInputs(int quantidade) {
        LinearLayout listaDeInputs = findViewById(R.id.lista_de_inputs_de_refeicao);

        listaDeInputs.removeAllViews();// limpa a lista de inputs pro caso de ser selecionado outra opção novamente

        for(int i = 0 ; i < quantidade; i++) {//for para criar leyouts conforme a quantidade escolhida no spinner
            LinearLayout layout = new LinearLayout(this);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            layout.setLayoutParams(params); // associação dos parâmetros criados anteriormente aos layouts criados

            EditText editNome = new EditText(this);
            final TextView editHorario = new TextView(this);

            editNome.setHint("Insira o nome da refeição");
            editHorario.setText("Horário");
            editHorario.setOnClickListener(new View.OnClickListener() { //ação de click do relógio
                @Override
                public void onClick(View view) {
                    mostrarRelogio(editHorario); //chamando o método que faz o relógio aparecer
                }
            });

            layout.addView(editNome); //adiciona todos os edits de nome no layout que foi criado
            layout.addView(editHorario);

            listaDeInputs.addView(layout); //adicionando o layout em um layout que vai receber vários layouts de acordo com a quantidade selecionada no spinner

            inputsNome.add(editNome); //adicionando os edits de nome no array list pra conseguir recuperar cada refeição associada a hora corretamente
            inputsHorario.add(editHorario);
        }
    }

    private void mostrarRelogio(final TextView horario) {
        TimePickerDialog relogio = new TimePickerDialog( //objeto que cria o relógio
                this, //contexto onde ele será mostrado
                R.style.PickerDialogTheme,
                new TimePickerDialog.OnTimeSetListener() { //ação de quando define um horario no relógio
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hora, int minuto) { // esse método aciona quando o horario for definido no relógio
                        horario.setText(hora + ":" + minuto); //pega os valores definidos (hora e minuto) e define no TextView da refeicao
                    }
                },
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY), //pega a hora atual do dia
                Calendar.getInstance().get(Calendar.MINUTE), //pega o minuto atual
                true
        );

        relogio.show(); //mostra o relógio
    }
}
