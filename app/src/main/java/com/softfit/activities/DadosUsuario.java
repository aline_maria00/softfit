package com.softfit.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.softfit.R;
import com.softfit.db.UsuarioDAO;
import com.softfit.modelagem.Usuario;

public class DadosUsuario extends AppCompatActivity {

    EditText nome;
    EditText idade;
    EditText peso;
    EditText altura;
    EditText imc;
    EditText localizacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_usuario);

        FloatingActionButton AdicionarDados = findViewById(R.id.fabSalvarUsuario);
        AdicionarDados.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                nome = findViewById(R.id.editTextNomeUsuario);
                idade = findViewById(R.id.editTextIdadeUsuario);
                peso = findViewById(R.id.editTextPesoUsuario);
                altura = findViewById(R.id.editTextAlturaUsuario);
                imc = findViewById(R.id.editTextIMCUsuario);
                localizacao = findViewById(R.id.editTextLocalizacaoUsuario);

                Usuario usuario = new Usuario(
                        nome.getText().toString(),
                        Integer.parseInt(idade.getText().toString()),
                        Float.parseFloat(peso.getText().toString()),
                        Float.parseFloat(altura.getText().toString()),
                        localizacao.getText().toString()
                );

                UsuarioDAO dao = new UsuarioDAO(DadosUsuario.this);
                dao.editarUsuario(usuario);

                finish();
            }
        });

    }
}

