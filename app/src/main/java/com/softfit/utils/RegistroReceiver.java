package com.softfit.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.softfit.db.RegistroDAO;

import java.util.Calendar;

public class RegistroReceiver extends BroadcastReceiver {

    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        if(intent.getAction() == "LIMPAR_REGISTRO") {
            new RegistroDAO(context).deletarTodosOsRegistros();
        }
    }

    public void limparMain() {
        Intent acao = new Intent("LIMPAR_REGISTRO");

        PendingIntent intentPendente = PendingIntent.getBroadcast(this.context, 0, acao, 0);

        Calendar calendario = Calendar.getInstance();
        calendario.set(Calendar.HOUR_OF_DAY, 23);
        calendario.set(Calendar.MINUTE, 59);
        calendario.set(Calendar.SECOND, 0);

        AlarmManager manager = (AlarmManager) this.context.getSystemService(Context.ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendario.getTimeInMillis(), intentPendente);
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            manager.setExact(AlarmManager.RTC_WAKEUP, calendario.getTimeInMillis(), intentPendente);
        }
        else {
            manager.set(AlarmManager.RTC_WAKEUP, calendario.getTimeInMillis(), intentPendente);
        }
    }
}
