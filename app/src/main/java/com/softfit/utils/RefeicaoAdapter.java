package com.softfit.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.softfit.R;
import com.softfit.modelagem.Refeicao;

import java.util.ArrayList;

//Classe adapter de refeicoes

public class RefeicaoAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Refeicao> refeicoes;

    public RefeicaoAdapter(Context contexto, ArrayList <Refeicao> refeicoes) {
        this.context = contexto;
        this.refeicoes = refeicoes;
    }

    @Override
    public int getCount() {
        return refeicoes.size();
    }

    @Override
    public Object getItem(int position) {
        return refeicoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return refeicoes.get(position).getId();
    }

    //getView vai renderizar o item do adaptador

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.item_do_list_view, null, false);

        TextView horaRefeicao = view.findViewById(R.id.horaRefeicao);
        TextView nomeRefeicao = view.findViewById(R.id.refeicao);

        ImageView foto = view.findViewById(R.id.imagemListView);
        horaRefeicao.setText(refeicoes.get(position).getHora());
        nomeRefeicao.setText(refeicoes.get(position).getNome());

        if(!refeicoes.get(position).getImg().equals("")) {
            Bitmap bitmap = BitmapFactory.decodeFile(refeicoes.get(position).getImg());

            foto.setImageBitmap(bitmap);
            foto.setTag(refeicoes.get(position).getImg());
        }

        return view;
    }

    public void atualizar(ArrayList<Refeicao> refeicoes) {
        this.refeicoes = refeicoes;
        this.notifyDataSetChanged();
    }
}
