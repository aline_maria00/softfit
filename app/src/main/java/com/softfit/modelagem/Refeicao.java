package com.softfit.modelagem;

import java.io.Serializable;

public class Refeicao implements Serializable {
    private int id;
    private String nome;
    private String hora;
    private String img;

    public Refeicao(int id, String nomeAlimento, String horaAlimento, String imagemAlimento) {
        this.id = id;
        this.nome = nomeAlimento;
        this.hora = horaAlimento;
        this.img = imagemAlimento;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Refeições {" +
                "Id = " + id +
                "Nome alimento = '" + nome + '\'' +
                '}';
    }
}
