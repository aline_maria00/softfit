package com.softfit.modelagem;

import java.io.Serializable;

public class Cardapio implements Serializable {
    private int id;
    private String descricao;
    private int refeicaoId;

    public Cardapio(int id, String descricao, int refeicaoId) {
        this.id = id;
        this.descricao = descricao;
        this.refeicaoId = refeicaoId;
    }

    public Cardapio(String descricao, int ref_id) {
        this.descricao = descricao;
        this.refeicaoId = ref_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getRefeicaoId() {
        return refeicaoId;
    }

    public void setRefeicaoId(int ref_id) {
        this.refeicaoId = ref_id;
    }

    @Override
    public String toString() {
        return "Cardápio {" +
                "Id = " + id +
                "Descrição = '" + descricao + '\'' +
                '}';
    }
}