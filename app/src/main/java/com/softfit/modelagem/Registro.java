package com.softfit.modelagem;

public class Registro {
    private int id;
    private String imagem;

    public Registro(int id, String imagem) {
        this.id = id;
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    @Override
    public String toString() {
        return "Registro: " +
                "Id = " + id +
                "Imagem = '" + imagem + '\'' +
                '}';
    }
}
